import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import NavMenu from './components/NavMenu';
import Author from './pages/Author';
import Home from './pages/Home';
function App() {
	return (
		<Router>
			<NavMenu />
			<Container>
				<Switch>
					{/* <Route exact path='/' component={Home} />
					<Route path='/detail/:id' component={ArticleDetail} />
					<Route path='/video' component={Video} />
					<Route path='/account' component={Account} />
					<ProtectedRoute path='/welcome'>
						<Welcome />
					</ProtectedRoute> */}
					<Route exact path='/' component={Home} />
					<Route path='/author' component={Author} />
				</Switch>
			</Container>
		</Router>
	);
}

export default App;
